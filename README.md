
# Death Markers mod for Minetest

Displays waypoints for some time after death to aid in finding the bones.

This mod:

- displays a waypoint for each death for a certain time (realtime)
- slowly fades waypoints from white through blue, green and red to black to indicate which marker is the most recent death and which are about to disappear. (the `waypoint_saturation` setting can be used to adjust the strength of the tint and even make it uniformly grey)
- death in creative doesn't produce bones, so those markers say "Death" instead of "Bones" and expire much more quickly
